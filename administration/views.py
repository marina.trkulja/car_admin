from django.shortcuts import render

# Create your views here.

def persons (request):
    return render(request, 'administration/myPersons.html')